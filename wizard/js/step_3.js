

let b11;
var start ="<div class='row'>"
var end ="</div>"
var message22 = "<div class='textfild'>Welches Material möchten Sie erstellen?</div><br/>"



var message_1 = "<div class='textfild'>Welche Nutzungsrechte werden bei dem fremden Material, auf das Sie zurückgreifen, eingeräumt?</div></br>"; 
var message_2 = "<div class='textfild'>Welche Nutzungsrechte möchten Sie anderen einräumen?</div></br>";
var q5map = new Map();

var q6map = new Map();


function replay_step_3(clicked_id, id) {

	tooltip();

	q5map.set("l1", license_1)
	q5map.set("l2", license_2)
	q5map.set("l3", license_3)
	q5map.set("l4", license_4)
	q5map.set("l5", license_5)
	q5map.set("l6", license_6)
	q5map.set("l7", license_7)
	q5map.set("l8", license_8)
	q5map.set("l9", license_9)

	q6map.set("l10", license_10)
	q6map.set("l11", license_11)
	q6map.set("l12", license_12)
	q6map.set("l13", license_13)
	q6map.set("l14", license_14)
	q6map.set("l15", license_15)
	q6map.set("l16", license_16)

	if(clicked_id == "q1"){	 
		result.set("c1", copyright_2)
	}else if(clicked_id == "q2"){
		result.set("c2", copyright_1)
	}else if(clicked_id == "q3"){	 
		result.set("c3", copyright_3)
	}

  
	var stepId = "step-"+id;


let rights = "";

	      if(clicked_id == "q1"){
	    	document.getElementById(stepId).innerHTML = "</br>"+ message_1 + "</br>";
					
			for (const [key, value] of q5map.entries()) {
				rights += userRights(key,value,1, "id_1")	
			}
			document.getElementById(stepId).innerHTML += start+rights+end;
			 navigation.set("nav2", q4map.get(clicked_id).toUpperCase())
			 document.getElementById("footer").innerHTML = footer + navigation.get("nav1").toUpperCase()+ " &#10141; " +q4map.get(clicked_id).toUpperCase();
	      }

		 else if(clicked_id == "q2" || clicked_id == "q3"){
	    	document.getElementById(stepId).innerHTML = "</br>"+ message_2 + "</br>";
	    
			for (const [key, value] of q6map.entries()) {

				if(clicked_id == "q2" ){
					rights += userRights(key,value,2,"id_2");
					
				} else if(clicked_id == "q3" ){
					rights += userRights(key,value,3,"id_2");
				} 
			}
		
			document.getElementById(stepId).innerHTML += start+rights+end;
			navigation.set("nav2", q4map.get(clicked_id).toUpperCase())
			document.getElementById("footer").innerHTML = footer + navigation.get("nav1").toUpperCase()+ " &#10141; " +q4map.get(clicked_id).toUpperCase();
			
	     }
	   
		
		$('#smartwizard').smartWizard("next");

}

function userRights(key,value,num,id){
	return  `<div id='${id}' class='col-lg-4 col-12 col-md-6'>
				<button id='${key}'
				onClick='replay_step_4(this.id, ${num})' 
				lic='${value['l1']}' 
				class='button'>${value['l1']}</button></div>`;
}