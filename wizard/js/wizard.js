
	function onCancel(){
		 $('#smartwizard').smartWizard("reset"); 
			document.getElementById("pdfbutton").innerHTML = "";
	}

	$(function() {
		// Step show event
		$("#smartwizard").on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
			$("#prev-btn").removeClass('disabled').prop('disabled', false);
			$("#next-btn").removeClass('disabled').prop('disabled', false);
			if(stepPosition === 'first') {
				$("#prev-btn").addClass('disabled').prop('disabled', true);
			} else if(stepPosition === 'last') {
				$("#next-btn").addClass('disabled').prop('disabled', true);
			} else {
				$("#prev-btn").removeClass('disabled').prop('disabled', false);
				$("#next-btn").removeClass('disabled').prop('disabled', false);
			}

			// Get step info from Smart Wizard
			let stepInfo = $('#smartwizard').smartWizard("getStepInfo");
			$("#sw-current-step").text(stepInfo.currentStep + 1);
			$("#sw-total-step").text(stepInfo.totalSteps);
			
		});
		
		// Smart Wizard
		$('#smartwizard').smartWizard({
			selected: 0,
			// autoAdjustHeight: false,
			theme: 'arrows', // basic, arrows, square, round, dots
			transition: {
			animation:'none'
			},
			toolbar: {
			showNextButton: true, // show/hide a Next button
			showPreviousButton: true, // show/hide a Previous button
			position: 'none' // none/ top/ both bottom
			},
			anchor: {
				enableNavigation: true, // Enable/Disable anchor navigation 
				enableNavigationAlways: false, // Activates all anchors clickable always
				enableDoneState: true, // Add done state on visited steps
				markPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
				unDoneOnBackNavigation: false, // While navigate back, done state will be cleared
				enableDoneStateNavigation: true // Enable/Disable the done state navigation
			},
			disabledSteps: [], // Array Steps disabled
			errorSteps: [], // Highlight step with errors
			hiddenSteps: []
		});
	
		$("#prev-btn-modal").on("click", function() {
			// Navigate previous
			$('#smartwizard').smartWizard("prev");
			return true;
		});

		$("#next-btn-modal").on("click", function() {
			// Navigate next
			$('#smartwizard').smartWizard("next");
			return true;
		});

		$("#state_selector").on("change", function() {
			$('#smartwizard').smartWizard("setState", [$('#step_to_style').val()], $(this).val(), !$('#is_reset').prop("checked"));
			return true;
		});

		$("#style_selector").on("change", function() {
			$('#smartwizard').smartWizard("setStyle", [$('#step_to_style').val()], $(this).val(), !$('#is_reset').prop("checked"));
			return true;
		});

	});

	var notify = function() {
	$.notify({
	// options
	title: '<strong style="font-weight: bold; font-size: 20px">Info</strong>',
	message: "<br>Mit diesem Planungstool generieren Sie in wenigen Schritten einen individuellen Leitfaden, der Sie dabei unterstützt qualitativ hochwertige Bildungsmaterialien im OER-Standard zu erstellen. Entsprechend Ihrer Auswahl erhalten Sie formale und didaktische Empfehlungen sowie Hinweise zu Lizenzangaben.",
	icon: 'glyphicon glyphicon-info-sign',
	}, {
	// settings
	element: 'body',
	position: null,
	type: "info",
	allow_dismiss: true,
	newest_on_top: true,
	showProgressbar: true,
	placement: {
		from: "top",
		align: "center"
	},
	z_index: 1070,
	delay: 6000,
	url_target: '_blank',
	mouse_over: null,
	animate: {
		enter: 'animated flipInY',
		exit: 'animated flipOutX'
	},
	onShow: null,
	onShown: null,
	onClose: null,
	onClosed: null,
	icon_type: 'image',
	template: '<div style="font-size:19px; font-family: work sans;" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
		'<button type="button" class="close btn" data-notify="dismiss">×</button>' +
		'<span data-notify="title">{1}</span>' +
		'<span data-notify="message">{2}</span>' +
		'</div>'

	});
	}

	function tooltip(){
		$(document).ready(function(){
			TOOLTIP_DATA.map((data)=> {
			$("#"+data.id).tooltip({
				title: data.title,
				html: true,
				trigger : 'hover',
			});
		  })
		});
	}