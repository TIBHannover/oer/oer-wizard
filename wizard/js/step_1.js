/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2022-07-20
 * @desc [description]
 */

var start ="<div class='row justify-content-md-center'>"
var end ="</div>"

var message2 = "<div class='textfild'>Welches Material möchten Sie erstellen?</div><br/>"

var footer = "<span style='color: #0A1F40; font-size: 13px; text-align: left;'>IHRE AUSWAHL: ";
 
var button1 = new Map();

 
let result = new Map();
 
var navigation = new Map();
 
 
 function replay_step_1() {
  
   button1.set("b1_1", b1_1_array);
   button1.set("b1_2", b1_2_array);
   button1.set("b1_3", b1_3_array);
   button1.set("b1_4", b1_4_array);
   button1.set("b1_5", b1_5_array);
   button1.set("b1_6", b1_6_array);
   button1.set("b1_7", b1_7_array);
   button1.set("b1_8", b1_8_array);
   button1.set("b1_9", b1_9_array);
   button1.set("b1_10", b1_10_array);
   button1.set("b1_11", b1_11_array);
   button1.set("b1_12", b1_12_array);
   button1.set("b1_13", b1_13_array);
   button1.set("b1_14", b1_14_array);
   button1.set("b1_15", b1_15_array);
   button1.set("b1_16", b1_16_array);
   button1.set("b1_17", b1_17_array);
   button1.set("b1_18", b1_18_array);
   button1.set("b1_19", b1_19_array);
   button1.set("b1_20", b1_20_array);
   button1.set("b1_21", b1_21_array);

    let b1="";

      tooltip();

     document.getElementById("step-1").innerHTML = "</br>"+ message2 + "</br>";
        for (const [key, value] of button1.entries()) {
          b1 += page_2(key, value, "b1_1",  2);     
        }
      document.getElementById("step-1").innerHTML += start+b1+end;
 }

  function page_2(key, value, index,  pagenumber){

      return `<div id="id_1" class="col-lg-4 col-12 col-md-6">
      <button class='button' onClick='replay_step_2(this.id, ${pagenumber})'
      id='${key}'
      tool='${value['tool']}' 
      heading='${value['heading']}' 
      intro='${value['intro']}' 
      dataformat='${value['dataformat']}' 
      text='${value[index]}' '>${value[index]}</button></div>`
  }

