/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */




function pdf() {
		
	var werkzeuge = htmlToPdfmake(result.get("tool"));
	var datenformate = htmlToPdfmake(result.get("dataformat"));
	var beschreibung = htmlToPdfmake(result.get("intro"));
	var nutzungs = htmlToPdfmake(copyright_result.get("copy"));
	var transparent = htmlToPdfmake(result.get("license"));


  // http://pdfmake.org/playground.html
  var docDefinition = {

//Seitenzahlen und Brotgrummen
    footer: function (currentPage, pageCount) {
      return {
        table: {
          widths: [40, '*', 40],
          body: [
            ['',
              {
                text: result.get("heading") + " | Seite " + currentPage.toString() + ' von ' + pageCount,
                alignment: 'right',
                style: 'normalText',
				fontSize: 8,
                margin: [0, 20, 0, 0]
              },
              ''
            ]
          ]
        },
        layout: {
          defaultBorder: false,
        },
      };
    },

    // volle Breite der Seite nutzen
    pageMargins: [0, 60, 0, 60],

    content: [

      {
        // if you specify width, image will scale proportionally
        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAAArCAYAAAAe/1QiAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABMNJREFUeNrsGk1r1EA0rdKDIE0RFYvSKMWLlgaKUERpSi892e290PYXdOvHebcH8bjWoyjZHgVh6y/I9uJ1V/QgHrYrguBBsiI9iV1nykscHzOZmTS7m8o8eGwyeTOZ9zHvK2tZBgwYMGDAgAED/zMMyQiGh095cNk8PPzdyRsDZH8u+SnAbZvgLm+fhM4GOvpbFfFC6NbIjwM07UEyVibYBWzkTOg2wYDZX4QhwQKHvsjQ7AvWZGnCnvMgeT7HXLs5M/qAoMcZp5ZdgxOBxyNwBGvaguuBCD+XAJYdC/fR1Iz1bNazbo6dY8lKaNp03vg4fUJj1T+Cf0iQwuJlx7r+uho9KiRYdT6M6KRnDLcvjsfXoyMj2PpFUBeMTxvha8Dzj+/j61etT9aH8LvFZD66wJ6OwWZ2OJvIkc932H1NnLW7dy5dwVlPOYGXQIHfYNCWb+fR2iH/Xo/uvxz8tN5++8qSNAk+RdO8k+bz3R5YrUsLN2q9x1QAjazLyL1QV0HH5yUFYSfDE1iGE9OITgxBnylOlRaqwEQW8VFmn5XR3KPihVfgsNWyShEDCgp57iOtQpNckoAmkAidJx+MDU69IX2xEgrm1hQr5q4qnUSoFVTdOhkJv5ZgQKGmrNayznY6PYwTOBUsKbpGO8N+zDtBnyvg8NiGPdcFcvF5CmCLLOo/N5AQXfSCHeZ+NwVD04r+vE42W2eC5Bq530zb2FM6+gq9JNq24BjgJsQf3JwroTYGVQBtTjZVXuZp+EBVOuXUlTKA1i2mXZPDS1HXNUEQZZ/vS9yhzQRhbnOy30WWcgYA1sRa+lKG+2gKLDvJ6rHbWE5ycXBK51E25rLuZ7gHzHgJDOjCbh/zdDchpuEMrqriPkAB62h4KWvhd7K0elHQ08qdszWoOfRsR+ME15H1ez11OwJf6CrSJbmHQVXcDkegabM3O2vh7yVtVmA9Irq+Vtx9gs9aeb6mdjtJQgJ/72Xkio5tuTloDNq6lu9puIcJxbm6/fM2YqLAUbTPCe5OxsIraE6Z4wTibNwO55Rg97AqmFqQZEGOSPiQstU4cYCOBymzKxG8QfcbmlbucXnQCXiSSpGbbkJQZS1lC80rqlpM9A5Y048Gz1y8YJ2fmsLKL2YofFzNe6J+Da+1IFpLJvyOxvGtC44m3mQVMLYinlJhjJ3L/h8nHh+fnbUWX76w7j55bM1sbqictjQnu432LOzXoArXR4ZH97+dyvIlVSbOfVdBASVUnOAekQ1uoggbtqH8DyRH/wgml+79DTQLC9botau9CrJbHGP0ob0Ru09ob9P9NziGt81WxTLh76n6aKj4mqgqxI2obSZGVJECKgRDwAry5XXcvIpNqdWKr38dHFg/WvvSQH0M618XJCKUzxB6Sw3Yv8Opisu6kR33rn2NZhiLFYXGk+iDhI1dGktzY2Wle+vB/e7Y5CS3H89prHkKzbe1BB51+/l+2rSqzFnMS6APVATIKCDpi5Awa1FQnMsGfdEztJdQJnzGtah8yQpFHVQdBfhoQVsWaFity9I+sDr2M2ZF1sdJ8V/N6Hurm9QWAZqColw8wedXXyUbGtJQgAt+uD3Qf+/y9yX9l7IBAwYMGDBgwICBfsMfAQYAKkE36H4AKbcAAAAASUVORK5CYII=',
        width: 100,
        absolutePosition: {
          x: 450,
          y: 55
        }
      },

      {
        svg: '<svg width="300" height="40" viewBox="0 0 400 400">...</svg>'
      },

//Tabellen sind dreispaltig, um vollbreite der Seite für HEadline und Kontakt zu nutzen
      {
        style: 'tableExample',
        table: {
          widths: ['*'],

          body: [
            [{
              margin: [10, 15, 10, 10],
              text: ' ',

            }, ],
            [{
                margin: [60, 15, 250, 15],
                fontSize: 18,
                text: result.get("heading"),
                fillColor: '#DDF0F0',
              },

            ],
            [{
              margin: [10, 0, 10, 0],
              text: ' ',

            }, ],
          ],

        },
        layout: {
          defaultBorder: false,
        },
      },
		
		
		
		
		
		//Verweis auf Planungshilfe
		{
        style: 'tableExample',
        table: {
          widths: ['*'],

          body: [
            [{
              margin: [10, 0, 10, 0],
              text: ' ',

            }, ],
            [{
                margin: [60, 0, 60, 0],
                fontSize: 10,
                text: "Ergebnis der OER-Planungshilfe\n(https://tibhannover.gitlab.io/oer/oer-wizard/html/wizard-modal.html) für das Szenario:\n\n"  + navigation.get("nav2").toUpperCase()
				//+ " — " +navigation.get("nav5").toUpperCase()
              },
            ],
            [{
              margin: [10, 0, 10, 0],
              text: ' ',

            }, ],
          ],

        },
        layout: {
          defaultBorder: false,
        },
      },
      {

		  
		  
		  
		  //Nutzungsbedingungen - korrekt
        table: {

          widths: [40, '*', 40],

          body: [


            ['',

              {
                margin: [10, 5, 5, 5],
                text: 'Schritt I: Nutzungsbedingungen prüfen',
                fillColor: '#FCDEE1',
                color: '#0A1F40',
                bold: true
              },
              '',
            ],


            ['',

              {
                margin: [10, 0, 0, 0],
                text: ' ',

              },
              '',
            ],


            ['',
              			nutzungs,
              '',
            ],

            ['',

              {
                margin: [10, 0, 0, 0],
                text: ' ',
              },
              '',
            ],

          ],


        },
        layout: {
          defaultBorder: false,
        },

      },

		
		
		// Schritt 2 — korrekt
      {

        table: {

          widths: [40, '*', 40],

          body: [
            ['',

              {
                margin: [10, 5, 5, 5],
                text: 'Schritt II: Werkzeug wählen (System oder Umgebung)',
                fillColor: '#FCDEE1',
                color: '#0A1F40',
                bold: true
              },
              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',

              },
              '',
            ],


            ['',

             werkzeuge,

              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',
              },
              '',

            ],


          ],


        },
        layout: {
          defaultBorder: false,
        },

      },


		//Schritt 3 - korrekt
   {
        table: {

          widths: [40, '*', 40],

          body: [
            ['',

              {
              margin: [10, 5, 5, 5],
              text: 'Schritt III: Formale und didaktische Hinweise beachten',
              fillColor: '#FCDEE1',
              color: '#0A1F40',
              bold: true
             },
              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',

              },
              '',
            ],


            ['',
         beschreibung,

              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',
              },
              '',

            ],


          ],


        },
        layout: {
          defaultBorder: false,
        },

      },

		
		//Schritt 4 - korrekt
   {
        table: {

          widths: [40, '*', 40],

          body: [
            ['',

              {
                margin: [10, 5, 5, 5],
                text: 'Schritt IV: Lizenzen transparent machen',
                fillColor: '#FCDEE1',
                color: '#0A1F40',
                bold: true
            },
              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',

              },
              '',
            ],


            ['',
        transparent,

              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',
              },
              '',

            ],


          ],


        },
        layout: {
          defaultBorder: false,
        },

      },

		
		
		
		//Schritt 5
      {
        table: {

          widths: [40, '*', 40],

          body: [
            ['',

              {
                margin: [10, 5, 5, 5],
                text: 'Schritt V: Bildungsmaterial speichern und bereitstellen',
                fillColor: '#FCDEE1',
                color: '#0A1F40',
                bold: true
             },
              '',
            ],


            ['',

              {
                margin: [10, 1, 0, 0],
                text: ' ',

              },
              '',
            ],


            ['',
           datenformate,

              '',
            ],


            ['',

             {
                margin: [10, 1, 0, 0],
                text: ' ',

              },
              '',

            ],


          ],


        },
        layout: {
          defaultBorder: false,
        },

      },


      //Kontakt:
      {
        style: 'tableExample',
        table: {
          widths: ['*'],

          body: [
            [{
              margin: [10, 15, 10, 10],
              text: ' ',

            }, ],
            [{
                margin: [60, 15, 60, 15],
                fontSize: 14,
				alignment: 'center',
				lineHeight: 1.3,
                text: `Teilen Sie Ihre OER:\nWir freuen uns, dass Sie sich dazu entschieden haben, eine OER zu erstellen und für andere zugänglich zu machen. Teilen Sie Ihre OER jetzt einfach auf dem OER-Portal twillo: https://www.twillo.de.\nOder beantragen Sie in einer kurzen E-Mail an support.twillo@tib.eu Ihren twillo-Account.`,
                fillColor: '#DDF0F0',
              },

            ],
            [{
              margin: [10, 15, 10, 10],
              text: ' ',

            }, ],
          ],

        },
        layout: {
          defaultBorder: false,
        },
      },
      //Lizenz:
      {
        style: 'tableExample',
        table: {
          widths: ['*'],

          body: [
            [{
              margin: [10, 15, 10, 10],
              text: ' ',

            }, ],
            [{
                margin: [120, 15, 120, 10],
                fontSize: 8,
				alignment: 'center',
                text: `Dieser Leitfaden der OER-Planungshilfe x OER-Plattform twillo ist lizenziert unter CC BY 4.0. (https://creativecommons.org/licenses/by/4.0/)\n\n ———————— Dieser Leitfaden stellt keine Rechtsberatung dar. ————————`,
              },

            ],
            [{
              margin: [10, 15, 10, 10],
              text: ' ',

            }, ],
          ],

        },
        layout: {
          defaultBorder: false,
        },
      },

    ],
	  styles:{
  
		  'html-p': {fontSize:'11', lineHeight:'1.3',
					},
		  'html-li': {fontSize:'11', lineHeight:'1.3'},
		  
		   'html-h4': {lineHeight:'1.9'},
  },
  };

  pdfMake.createPdf(docDefinition).open();
}