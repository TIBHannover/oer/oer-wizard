/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2022-07-20
 * @desc [description]
 */
 var knowledge = "<p>Dieser Leitfaden der <a href='https://tibhannover.gitlab.io/oer/oer-wizard/html/wizard-modal.html' target='_blank' rel='noopener noreferrer'> OER-Planungshilfe</a> x OER-Plattform twillo ist lizenziert unter <a href='https://creativecommons.org/licenses/by/4.0/' target='_blank' rel='noopener noreferrer'>CC BY 4.0</a>.<br><br><strong>Dieser Leitfaden stellt keine Rechtsberatung dar.</strong>";

 var message = "<center><b>Teilen Sie Ihre OER.</b></center></br>Wir freuen uns, dass Sie sich dazu entschieden haben, eine OER zu erstellen und für andere zugänglich zu machen. Teilen Sie sie jetzt einfach auf dem OER-Portal twillo oder beantragen Sie in einer kurzen E-Mail an <a href='mailto:support.twillo@tib.eu' target='_blank' rel='noopener noreferrer'>support.twillo@tib.eu</a> Ihren twillo-Account.";
 
 let copyright_result = new Map();
 
 function replay_step_4(clicked_id, co) {

	 	tooltip();

		result_content(clicked_id,co)

		 $('#smartwizard').smartWizard("next");
	}



function result_content(clicked_id,co){

	var buttonselect = queryId(clicked_id);

	var attr = buttonselect.getAttributeNode("lic").value;
	
	var cc0map = new Map();
	cc0map.clear();
	if(clicked_id == "l1"){
		cc0map.set("cc0",cc0_a)
	}
	if(clicked_id == "l2"){
		cc0map.set("cc0",cc_by)
	}
	if(clicked_id == "l3"){
		cc0map.set("cc0",cc_by_sa)
	}
	if(clicked_id == "l4"){
		cc0map.set("cc0",cc_by_nd)
	}
	if(clicked_id == "l5"){
		cc0map.set("cc0",cc_by_nc)
	}
	if(clicked_id == "l6"){
		cc0map.set("cc0",cc_by_nc_sa)
	}
	if(clicked_id == "l7"){
		cc0map.set("cc0",cc_by_nc_nd)
	}
	if(clicked_id == "l8"){
		cc0map.set("cc0",cc_pd_mark)
	}
	if(clicked_id == "l9"){
		cc0map.set("cc0",copy1)
	}


	if(clicked_id == "l10"){
		cc0map.set("cc0",cc0_b)
	}
	if(clicked_id == "l11"){
		cc0map.set("cc0",cc_by_4)
	}
	if(clicked_id == "l12"){
		cc0map.set("cc0",cc_by_sa_4)
	}
	if(clicked_id == "l13"){
		cc0map.set("cc0",cc_by_nd_4)
	}
	if(clicked_id == "l14"){
		cc0map.set("cc0",cc_by_nc_4)
	}
	if(clicked_id == "l15"){
		cc0map.set("cc0",cc_by_nc_sa_4)
	}
	if(clicked_id == "l16"){
		cc0map.set("cc0",cc_by_nc_nd_4)
	}


	result.set("license", cc0map.get("cc0"))
	var copy = "c" + co;

	copyright_result.set("copy", result.get(copy));
	
	var heading = `<div class='statictext_2'>${result.get("heading")}</div></br></br></br>`;

	let b1 = create_1("Schritt I: Nutzungsbedingungen prüfen", "paragraph.svg", 150, 150, text_2, copy, true);

	let b2 = create_1("Schritt II: Werkzeug wählen (System oder Umgebung)", "image_3.svg", 150, 150, text_3, "tool", true);

	let b3 = create_1("Schritt III: Formale und didaktische Hinweise beachten", "image_1.svg", 150, 150, text_4, "intro", true);

	let b4 = create_1("Schritt IV: Lizenzen transparent machen", "image_5.svg", 150, 150, text_5, cc0map.get("cc0"), false);

	let b5 = create_1("Schritt V: Bildungsmaterial speichern und bereitstellen", "image_4.svg", 150, 150, text_6, "dataformat", true);

	//let b6 = create_2("Wissenswertes", "image_6.svg", 150, 150, text_7 , false);

	let b7 = create_3(text_7, "image_8.svg", 150, 150);


   	navigation.set("nav5", attr)

   	creatHTML(heading, text_1, b1, b2, b3, b4, b5, b7);

	document.getElementById("footer").innerHTML = footer + navigation.get("nav1").toUpperCase()+ " &#10141; " +
	navigation.get("nav2").toUpperCase()+ " &#10141; " +navigation.get("nav5").toUpperCase();
	document.getElementById("pdf").innerHTML = footer_div;
		  
}	
	
function create_1(stepname, image, width, height, text, key, cc0){

	return `<div class='row'><div  class='col-8'><span  class='span'>${stepname}</span></div></div></br></br></br></br>
	<div class='row'><div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>
	<img class='imagelable' src='../image/wizard/${image}' width='${width}' height='${height}'></div>
	<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'><div class='span1'>${text}</div></div></div></br></br>
	<div class='row'><div class='col-8'><span class='labletext'>${cc0 ? result.get(key) : key}</span></div></div></br></br>`

}

function create_2(stepname, image, width, height, text, key, cc0){

	return `<div class='row'><div  class='col-8'><span  class='span'>${stepname}</span></div></div></br></br>
	<div class='row' style="margin-left: 43px"><div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>
	<img class='imagelable' src='../image/wizard/${image}' width='${width}' height='${height}'></div>
	<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'><div class='span1'>${text}</div></div></div></br></br>`

}

function create_3(){

	return `<div class='row'>
				<div style=' border-radius: 10px; margin-left: 1%; margin-right: 1%;  class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>
							<div style='font-family: work sans; line-height: 1.6; padding: 8%; margin-bottom: -4%; margin-top: -2%'><img  style='margin-top: 0%; margin-right: 5%;' src='../image/wizard/by.png' width='150' height='52'><br><br> ${knowledge}
							</div>
						</div></div></br></br></br>
				<div class='row'>
					<div class='col-12'>
						<div class='message2'><p>${message}</p></div>
					</div>
				</div></br>`;

}

function creatHTML(heading, text_1, b1, b2, b3, b4, b5, b7){
	 document.getElementById("step-4").innerHTML = " <div class='googoose-wrapper'></br><center><img id='image-9' src='../image/wizard/image_9.svg' width='500' height='250'></center><div class='statictext'>"+text_1+"</div></br></br>"+"<div class='container'>"+heading+"</br>"+b1+b2+b3+b4+b5+b7+"</div></div>";
	 document.getElementById("footer").setAttribute("style", "height: 8%; float: left; width: 100%; text-align: left;"); 
}

function exportDoc() {
        var o = {
            filename: 'Leitfaden_OER-Planunghilfe-x-twillo.doc'
        };
        $(document).googoose(o);
    };

var footer_div = "<div id='pdfbutton'><button  style='box-shadow: 0px 3px 5px rgb(10, 31, 64, 0.2)' class='btn btn-secondary' type='button' onClick='replay_step_5()'>PDF &#10515;</button> <button style='box-shadow: 0px 3px 5px rgb(10, 31, 64, 0.2)' class='btn btn-secondary' type='button' onclick='exportDoc()'>Doc &#10515;</button></div>";

